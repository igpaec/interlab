from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import numpy as np
from pyevtk.hl import gridToVTK

fig = plt.figure()
ax = fig.gca(projection='3d')

x_cor = np.arange(-1, 1, 0.1)
y_cor = np.arange(-1, 1, 0.1)
z_cor = np.arange(-1, 1, 0.1)

x, y, z = np.meshgrid(x_cor, y_cor, z_cor)

u = np.sin(np.pi * x) * np.cos(np.pi * y) * np.cos(np.pi * z)
v = -np.cos(np.pi * x) * np.sin(np.pi * y) * np.cos(np.pi * z)
w = (np.sqrt(2.0 / 3.0) * np.cos(np.pi * x) * np.cos(np.pi * y) *
     np.sin(np.pi * z))

# ax.quiver(x, y, z, u, v, w, length=0.1)
#
# plt.show()

nx, ny, nz = x.shape

h = 0
k = 0
l = 0

phi = ((x)**2) + ((y)**2) + ((z)**2) - 1


with open('data_demo03.vtr', 'w') as writer:
    writer.write('# vtk DataFile Version 2.0\n')
    writer.write('Sample rectilinear grid\n')
    writer.write('ASCII\n')
    writer.write('DATASET RECTILINEAR_GRID\n')
    writer.write(f'DIMENSIONS {nx} {ny} {nz}\n')
    writer.write(f'X_COORDINATES {nx} float\n')

    for i in x_cor:
        writer.write(f'{i} ')

    writer.write(f'\n')
    writer.write(f'Y_COORDINATES {ny} float\n')

    for j in y_cor:
        writer.write(f'{j} ')

    writer.write(f'\n')
    writer.write(f'Z_COORDINATES {nz} float\n')

    for k in z_cor:
        writer.write(f'{k} ')

    writer.write(f'\n')
    writer.write(f'POINT_DATA {nx * ny * nz}\n')
    writer.write(f'SCALARS scalars_ipa float\n')
    writer.write(f'LOOKUP_TABLE default\n')

    number_of_points = nx * ny * nz
    for i in range(nx):
        for j in range(ny):
            writer.write(f'{phi[i][j]}\n')

    for i in range(nx):
        for j in range(ny):
            for k in range(nz):
                writer.write(f'{phi[i][j][k]}\n')

    writer.write(f'\n')
    writer.write(f'VECTORS vectors_ipa float\n')

    for i in range(nx):
        for j in range(ny):
            for k in range(nz):
                writer.write(f'{u[i][j][k]} {v[i][j][k]} {w[i][j][k]}\n')
