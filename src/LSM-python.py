import numpy as np
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import time, sys
import math
import copy

#parameter

x0 = 0
x1 = 1
y0 = x0
y1 = x1
dx = 0.01
dy = dx

#Circule
#r = 0.15
#cx = 0.5
#cy = 0.75

#fig = pyplot.figure()
#ax = fig.gca(projection='3d')

x = np.arange(x0,x1,dx)
y = np.arange(y0,y1,dy)


#x = np.reshape(x,(100,100))
#y = np.reshape(y, (100,100))

#fig = pyplot.figure()
#ax = fig.add_subplot(111)

X,Y = np.meshgrid(x,y,indexing='ij')

x_len = len(X)
y_len = len(Y)

phi = (((X-.5)**2 +(Y-.75)**2)**0.5)-.15

#breaks = np.arange(0,1,0.01)
#grafica 3d

#ax.plot_surface(X, Y, phi, rstride=10, cstride=10, alpha=0.3)#surface
#cset = ax.contour(X, Y, phi, zdir='phi', offset=0, cmap=cm.coolwarm, levels=[0.01])
################
#cset = ax.contour(X, Y, phi, zdir='x', offset=0, cmap=cm.coolwarm)
#cset = ax.contour(X, Y, phi, zdir='y', offset=0, cmap=cm.coolwarm)
#ax.set_xlabel('X')
#ax.set_xlim(0, 1)
#ax.set_ylabel('Y')
#ax.set_ylim(0, 1)
#ax.set_zlabel('phi')
#ax.set_zlim(0, 1)

#pyplot.show()
#cs = pyplot.contour(X,Y,phi, levels=[0.01], extend='neither') #levels=[0.01]

#pyplot.show()

#1.-Configure vector field

u = (math.pi/314)*(.5-Y)
v = (math.pi/314)*(X-.5)

#pyplot.quiver(X,Y,u,v)

#pyplot.show()

#2.-Configure vector field


x0 = 0
x1 = 1
y0 = x0
y1 = x1

xx_low = np.arange(x0, x1, 0.1)
yy_low = np.arange(y0, y1, 0.1)
#zz_low = np.arange(y0, y1, 0.1)
x_low, y_low= np.meshgrid(xx_low, yy_low)
u_low = (math.pi / 314) * (.5 - y_low)
v_low = (math.pi / 314) * (x_low - .5)
#w_low = ((math.pi / 314) * (.5 - y_low))*((math.pi / 314) * (.5 - x_low))*((math.pi / 314) * (.5 - z_low))
#ax.plot(xx_low, v_low, zs = 0, zdir='z')#y_low, z_low, u_low, v_low, w_low)
pyplot.quiver(x_low, y_low, u_low, v_low)
# pyplot.show()

#Evolution

tNow = 0
#tMax = 628
tMax = 628
dt = 0.1
phi_new = np.zeros((x_len, y_len))
counter = 0


while tNow < tMax:
    for i in range(1,x_len-1):
        for j in range(1, y_len-1):
            if u[i,j]< 0:
                Dx =(phi[i+1,j]-phi[i,j])/dx
            if u[i,j] > 0:
                Dx = (phi[i,j]-phi[i-1,j])/dy
            if v[i,j]< 0:
                Dy = (phi[i, j+1]-phi[i,j])/dx
            if v[i,j] > 0:
                Dy = (phi[i,j]-phi[i,j-1])/dy

            space_diff = u[i,j]*Dx + v[i,j]*Dy
            phi_new[i,j] = phi[i,j]-space_diff * dt

    phi = copy.deepcopy(phi_new)
    phi_new = np.zeros((x_len, y_len))
    tNow = tNow + dt
    counter += 1

    if counter % 200:
        print(f't_now = {tNow}')
        pyplot.contour(X, Y, phi, [0])
        pyplot.quiver(X, Y, u, v)
        #pyplot.show()

        pyplot.savefig(f'../output/my_fig{counter:05d}.png')

        pyplot.clf()


#Ejemplos de graficas de funciones.
""""
x = np.arange(-2, 2, 0.01);
p = np.arange(-2, 2, 0.01);
X, P = np.meshgrid(x, p)
h = (P ** 2) / (2 * (1 + X ** 2)) + X ** 2 / 2.0

cs = pyplot.contour(X, P, h, levels=[.300])
#pyplot.clabel(cs, inline=1, fontsize=10)

pyplot.show()
"""""
""""
fig = pyplot.figure()
ax = fig.add_subplot(111)

x = y = np.linspace(-5, 5, 100)

X,Y = np.meshgrid(x,y)
Z = (-4*X)/(X**2 + Y**2 + 1)

cs = ax.contour(X,Y,Z, levels = [1])
pyplot.show()

"""
"""
x = np.arange(-5, 5, 0.1)
y = np.arange(-5, 5, 0.1)
xx, yy = np.meshgrid(x, y, sparse=True)
z = np.sin(xx**2 + yy**2) / (xx**2 + yy**2)
h = pyplot.contourf(x,y,z, levels=[0.1,0.5])

pyplot.show()"""
