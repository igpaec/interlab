# Example: Basic Level Set Method
# Author: Israel Pineda, Yachay Tech University
# Date: 2019-03-20
#
# Description: This is a basic, self-contained example of the
# implementation of the Level Set Method as described by Osher and
# Fedkiw[1]. It is intended to be as basic as possible and is useful only
# for didactic purposes.
#
# It exemplifies (albeit very simplistically):
# 1. creation of the grid
# 2. phi initialization
# 3. External velocity creation
# 4. Discretization of the Level Set Equation
# 5. Visualization
#
# The discretization of the level set equations uses Euler step in time and
# first order upwind differentiation in space. The time step is fixed. We
# display the evolution in every time step so the user can see how the
# method works. If you are studying this file you don't need to worry about
# any of the other files or directories in this library, it is completely
# self-contained.
#
# WARNING: This method is extremely basic, it will NOT provide an accurate
# solution for any kind of serious work. See the other examples for more
# information.
#
# [1] S. J. Osher, Level Set Methods and Dynamic Implicit Surfaces. 2002.

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

from skimage import measure
from skimage.draw import ellipsoid

import math
from copy import deepcopy


# nx, ny = (3, 2)
# x = np.linspace(0, 1, nx)
# y = np.linspace(0, 1, ny)
# xv, yv = np.meshgrid(x, y)
# print(xv)
# print(yv)


# # Parameters
x0 = 0
x1 = 1
y0 = x0
y1 = x1
dx = 0.01
dy = dx

xx = np.arange(x0, x1, dx)
yy = np.arange(y0, y1, dy)
# x, y = np.meshgrid(xx, yy)
# x, y = np.meshgrid(xx, yy, indexing='xy')
x, y = np.meshgrid(xx, yy, indexing='ij')

x_len = len(xx)
y_len = len(yy)
# Distance function init:  sin
# z = np.sin(x**2 + y**2) / (x**2 + y**2)

# Distance function init:  circle
cx = 0.5
cy = 0.75
radius = 0.15
phi = ((x - cx)**2 + (y - cy)**2)**0.5 - radius
# phi = ((x - cx). ^ 2 + (y - cy). ^ 2). ^ .5 - radius;


# radius = 0.15;
# cx = 0.5;
# cy = 0.75;

# Plot the surface
# fig = plt.figure()
# ax = fig.gca(projection='3d')
# surf = ax.plot_surface(x, y, z, cmap=cm.coolwarm,
#                        linewidth=0, antialiased=False)

# # Customize the z axis.
# # ax.set_zlim(-1.01, 1.01)
# # ax.zaxis.set_major_locator(LinearLocator(10))
# # ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
#
# # Add a color bar which maps values to colors.
# fig.colorbar(surf, shrink=0.5, aspect=5)
# plt.show()

# Plot the contour
plt.contour(x, y, phi, [0])
plt.show()


# # Configure vector field
vel_x0 = 0.5
vel_y0 = 0.5
u = (math.pi / 314) * (vel_y0 - y)
v = (math.pi / 314) * (x - vel_x0)

# plt.quiver(x, y, u, v)

x0 = 0
x1 = 1
y0 = x0
y1 = x1

xx_low = np.arange(x0, x1, 0.1)
yy_low = np.arange(y0, y1, 0.1)
x_low, y_low = np.meshgrid(xx_low, yy_low)
u_low = (math.pi / 314) * (vel_y0 - y_low)
v_low = (math.pi / 314) * (x_low - vel_x0)
plt.quiver(x_low, y_low, u_low, v_low)
# plt.quiver(x, y, u, v)
# plt.Axes().set_aspect(1.0)
plt.axes().set_aspect('equal', 'datalim')
plt.show()

# Evolution
tNow = 0
tMax = 628
# tMax = 10
dt = 0.1
phi_new = np.zeros((x_len, y_len))
# space_diff = np.zeros((x_len, y_len))
counter = 0

while tNow < tMax:
    # for i=2:100
    #     for j=2:100
    for i in range(1, x_len-1):
        for j in range(1, y_len-1):
            if u[i, j] < 0:  # Upwinding condition
                # Forward difference
                Dx = (phi[i + 1, j] - phi[i, j]) / dx

            if u[i, j] > 0:  # Upwinding condition
                # Backward difference
                Dx = (phi[i, j] - phi[i - 1, j]) / dx

            if v[i, j] < 0:  # Upwinding condition
                # Forward difference
                Dy = (phi[i, j + 1] - phi[i, j]) / dy

            if v[i, j] > 0:  # Upwinding condition
                # Backward difference
                Dy = (phi[i, j] - phi[i, j - 1]) / dy

            space_diff = u[i, j] * Dx + v[i, j] * Dy
            phi_new[i, j] = phi[i, j] - space_diff * dt

    phi = deepcopy(phi_new)
    phi_new = np.zeros((x_len, y_len))
    tNow = tNow + dt
    counter += 1

    if counter % 100:
        # # 2D contour
        # plt.contour(x, y, phi, [0])
        # # plt.quiver(x_low, y_low, u_low, v_low)
        # plt.quiver(x, y, u, v)
        # plt.axes().set_aspect('equal', 'datalim')
        # plt.show()

        # 3D surface
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        surf = ax.plot_surface(x, y, phi, cmap=cm.coolwarm,
                               linewidth=0, antialiased=False)

        # Customize the z axis.
        ax.set_zlim(-1.01, 1.01)
        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
        fig.colorbar(surf, shrink=0.5, aspect=5)

        plt.savefig(f'../output/my_fig{counter:05d}.png')

        plt.clf()

    print(f't_now = {tNow}')
























# Write to OBJ file
# # Generate a level set about zero of two identical ellipsoids in 3D
# ellip_base = ellipsoid(6, 10, 16, levelset=True)
# ellip_double = np.concatenate((ellip_base[:-1, ...],
#                                ellip_base[2:, ...]), axis=0)
# verts, faces, normals, values = measure.marching_cubes_lewiner(ellip_double, 0)
# faces=faces +1
#
# thefile = open('test.obj', 'w')
# for item in verts:
#   thefile.write("v {0} {1} {2}\n".format(item[0],item[1],item[2]))
#
# for item in normals:
#   thefile.write("vn {0} {1} {2}\n".format(item[0],item[1],item[2]))
#
# for item in faces:
#   thefile.write("f {0}//{0} {1}//{1} {2}//{2}\n".format(item[0],item[1],item[2]))
#
# thefile.close()
#
# plt.show()




# fig = plt.figure()
# ax = fig.gca(projection='3d')
#
# # Make data.
# X = np.arange(-5, 5, 0.25)
# Y = np.arange(-5, 5, 0.25)
# X, Y = np.meshgrid(X, Y)
# R = np.sqrt(X**2 + Y**2)
# Z = np.sin(R)
#
# # Plot the surface.
# surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
#                        linewidth=0, antialiased=False)
#
# # Customize the z axis.
# ax.set_zlim(-1.01, 1.01)
# ax.zaxis.set_major_locator(LinearLocator(10))
# ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
#
# # Add a color bar which maps values to colors.
# fig.colorbar(surf, shrink=0.5, aspect=5)
#
# plt.show()


# # Parameters
# x0 = 0;
# x1 = 1;
# y0 = x0;
# y1 = x1;
# dx = 0.01;
# dy = dx;

# Full grid
# [x, y] = ndgrid((x0:dx:x1), (y0:dx:y1));
# [x, y] = ndgrid((x0:dx:x1), (y0:dx:y1));

# # Circle
# radius = 0.15;
# cx = 0.5;
# cy = 0.75;

# phi = ((x - cx). ^ 2 + (y - cy). ^ 2). ^ .5 - radius;

# # Configure vector field
# vel_x0 = 0.5;
# vel_y0 = 0.5;
# u = (pi / 314) * (vel_y0 - y);
# v = (pi / 314) * (x - vel_x0);

# # Evolution
# tNow = 0;
# tMax = 628;
# dt = 0.1;
# phi_new = zeros(size(x));

# while tNow < tMax:
#     for i=2:100
#         for j=2:100
#             if u(i, j) < 0: # Upwinding condition
#                 # Forward difference
#                 Dx = (phi(i + 1, j) - phi(i, j)) / dx;
#
#             if u(i, j) > 0: # Upwinding condition
#                 # Backward difference
#                 Dx = (phi(i, j) - phi(i - 1, j)) / dy;
#
#             if v(i, j) < 0: # Upwinding condition
#                 # Forward difference
#                 Dy = (phi(i, j + 1) - phi(i, j)) / dx;
#
#             if v(i, j) > 0: # Upwinding condition
#                 # Backward difference
#                 Dy = (phi(i, j) - phi(i, j - 1)) / dy;
#
#             space_diff = u(i, j) * Dx + v(i, j) * Dy;
#             phi_new(i, j) = phi(i, j) - space_diff * dt;
#
# phi = phi_new;
# phi_new = zeros(size(x));
# tNow = tNow + dt

# contour(x, y, phi, [0 0], 'r', 'linewidth', 1);