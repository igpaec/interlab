import vtk
import numpy as np
from src.export_to_vtk import export_to_vtk


x_cor = np.arange(-1, 1, 0.1/2)
y_cor = np.arange(-1, 1, 0.1/2)
z_cor = np.arange(-1, 1, 0.1/2)

x, y, z = np.meshgrid(x_cor, y_cor, z_cor)

# u = np.sin(np.pi * x) * np.cos(np.pi * y) * np.cos(np.pi * z)
# v = -np.cos(np.pi * x) * np.sin(np.pi * y) * np.cos(np.pi * z)
# w = (np.sqrt(2.0 / 3.0) * np.cos(np.pi * x) * np.cos(np.pi * y) *
#      np.sin(np.pi * z))

# ax.quiver(x, y, z, u, v, w, length=0.1)
#
# plt.show()

nx, ny, nz = x.shape

h = 0
k = 0
l = 0

phi_sphere1 = ((x)**2) + ((y)**2) + ((z)**2) - 0.5
# phi_sphere2 = ((x)**2) + ((y)**2) + ((z+0.8)**2) - 0.5
phi_sphere2 = ((x)**2) + ((y)**2) + ((z+0.0)**2) - 0.5

# v = 0
# h = 0
# a = 0
# phi_cylinder = (x+a)**2 + (y-a)**2 - 0.3

# phi_union = np.minimum(phi_sphere1, phi_sphere2)
phi_union = np.maximum(phi_sphere1, phi_sphere2)

phi_p1 = ((x)**2)+((y-0.3)**2)-(z)
phi_p2 = ((x)**2)+((y)**2)+(z-0.5)
phi_p3 = np.maximum(phi_p1, phi_p2)


export_to_vtk(x_cor, y_cor, z_cor, x, y, z, phi_p3)

points = vtk.vtkPoints()


for i in range(nx):
    for j in range(ny):
        for k in range(nz):
            # if -0.2<phi[i][j][k]< -0.1:
            if phi_p3[i][j][k]< 0.0:
                array_point = np.array([x[i][j][k], y[i][j][k], z[i][j][k]] )
                points.InsertNextPoint(x[i][j][k], y[i][j][k], z[i][j][k])

poly = vtk.vtkPolyData()
poly.SetPoints(points)

# To create surface of a sphere we need to use Delaunay triangulation
d3D = vtk.vtkDelaunay3D()
d3D.SetInputData( poly ) # This generates a 3D mesh

# We need to extract the surface from the 3D mesh
dss = vtk.vtkDataSetSurfaceFilter()
dss.SetInputConnection( d3D.GetOutputPort() )
dss.Update()

# Now we have our final polydata
spherePoly = dss.GetOutput()

mapper = vtk.vtkPolyDataMapper()
mapper.SetInputData(spherePoly)

actor = vtk.vtkActor()
actor.SetMapper(mapper)

ren = vtk.vtkRenderer()
ren.AddActor(actor)
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)

iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

renWin.Render()
iren.Start()
