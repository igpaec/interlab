# This is a repository of several interesting and/or useful distance functions.


def sphere(x, y, z, a, b, c, r):
    """
    Returns the distance function of a sphere of the form (x - a)² + (y - b)² + (z - c)² = r²
    """
    s = ((x-a)**2) + ((y-b)**2) + ((z-c)**2) - r**2

    return s
