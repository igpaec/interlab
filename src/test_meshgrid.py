import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

nx, ny = (3, 2)
x = np.linspace(0, 1, nx)
y = np.linspace(0, 1, ny)
xv, yv = np.meshgrid(x, y)

z = xv + yv

print(z)

# Surface
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# 
# ax.plot_surface(xv, yv, z)
# 
# ax.set_xlabel('X Label')
# ax.set_ylabel('Y Label')
# ax.set_zlabel('Z Label')
# 
# plt.show()


# Bar plot
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')


# top = x + y
# bottom = np.zeros_like(z)
width = 0.3
depth = 0.5

x1 = xv.ravel()
y1 = yv.ravel()
z1 = x1 + y1
bottom = np.zeros_like(z1)
ax.bar3d(x1, y1, bottom, width, depth, z1, shade=True)
# ax.set_title('Shaded')

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

plt.show()
