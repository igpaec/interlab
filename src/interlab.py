# This is a project developed mainly at Yachay Tech University - Ecuador
# The project is done by the Scientific Computing Group at Yachay Tech.
# The goal is to create a simulation pipeline for the modeling, simulation, visualization and rendering of the
#  Navier-Stokes using Level Set Method.
#
# The core components are:
# - Solver of the Navier-Stokes equations
# - Level Set Method framework
# - Scientific Visualization
# - Rendering


def main():
    print(f'Welcome to InterLab')

    # read_config()
    # execute()


if __name__ == '__main__':
    main()
